use crate::couchdb::Document;
use serde_json::json;

pub fn views() -> Vec<Document> {
    vec![
        json!({
            "_id": "_design/find_newest",
            "language": "query",
            "views": {
                "events": {
                    "map": {
                        "fields": {
                            "created_at": "desc",
                            "ty": "desc"
                        },
                        "partial_filter_selector": {}
                    },
                    "reduce": "_count",
                    "options": {
                        "def": {
                            "fields": [
                                "created_at",
                                "ty"
                            ]
                        }
                    }
                }
            }
        })
        .into(),
        json!({
            "_id": "_design/find_by_nid",
            "language": "query",
            "views": {
                "events": {
                    "map": {
                        "fields": {
                            "nid": "desc"
                        },
                        "partial_filter_selector": {}
                    },
                    "reduce": "_count",
                    "options": {
                        "def": {
                            "fields": [
                                "nid"
                            ]
                        }
                    }
                }
            }
        })
        .into(),
        json!({
            "_id": "_design/find_by_id",
            "language": "query",
            "views": {
                "events": {
                    "map": {
                        "fields": {
                            "id": "desc"
                        },
                        "partial_filter_selector": {}
                    },
                    "reduce": "_count",
                    "options": {
                        "def": {
                            "fields": [
                                "id"
                            ]
                        }
                    }
                }
            }
        })
        .into(),
        json!({
            "_id": "_design/find_by_path",
            "language": "query",
            "views": {
                "events": {
                    "map": {
                        "fields": {
                            "path": "desc"
                        },
                        "partial_filter_selector": {}
                    },
                    "reduce": "_count",
                    "options": {
                        "def": {
                            "fields": [
                                "path"
                            ]
                        }
                    }
                }
            }
        })
        .into(),
        json!({
            "_id": "_design/find_events",
            "language": "query",
            "views": {
                "events": {
                    "map": {
                        "fields": {
                            "start_at": "asc"
                        },
                        "partial_filter_selector": {}
                    },
                    "reduce": "_count",
                    "options": {
                        "def": {
                            "fields": [
                                "start_at"
                            ]
                        }
                    }
                }
            }
        })
        .into(),
        json!({
            "_id": "_design/find_transactions",
            "language": "query",
            "views": {
                "transactions": {
                    "map": {
                        "fields": {
                            "year": "asc"
                        },
                        "partial_filter_selector": {}
                    },
                    "reduce": "_count",
                    "options": {
                        "def": {
                            "fields": [
                                "year"
                            ]
                        }
                    }
                }
            }
        })
        .into(),
        json!({
            "_id": "_design/find_transactions_by_membership",
            "language": "query",
            "views": {
                "transactions": {
                    "map": {
                        "fields": {
                            "membership_id": "asc"
                        },
                        "partial_filter_selector": {}
                    },
                    "reduce": "_count",
                    "options": {
                        "def": {
                            "fields": [
                                "membership_id"
                            ]
                        }
                    }
                }
            }
        })
        .into(),
        json!({
            "_id": "_design/transaction",
            "views": {
                "years": {
                "reduce": "_count",
                "map": "function (doc) {\n  if (doc._id.indexOf(\"transaction_\") === 0) {\n    emit(doc.year);\n  }\n}"
                }
            },
            "language": "javascript"
        })
        .into(),
        json!({
            "_id": "_design/find_deposits_by_membership",
            "language": "query",
            "views": {
                "deposits": {
                    "map": {
                        "fields": {
                            "membership_id": "asc",
                            "date": "asc"
                        },
                        "partial_filter_selector": {}
                    },
                    "reduce": "_count",
                    "options": {
                        "def": {
                            "fields": [
                                "membership_id",
                                "date"
                            ]
                        }
                    }
                }
            }
        })
        .into(),
    ]
}
