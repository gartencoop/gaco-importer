mod couchdb;
mod opts;
mod phone;
mod sql;
mod taxonomy;
mod views;

use anyhow::Error;

pub use opts::OPTS;
pub use sql::*;
pub use taxonomy::Taxonomy;

fn main() -> Result<(), Error> {
    env_logger::init();

    #[cfg(feature = "upload")]
    let mut old_documents = couchdb::get_all()?;

    #[allow(unused_variables, unused_mut)]
    let mut data = sql::data(&old_documents)?;

    #[cfg(feature = "upload")]
    {
        use indicatif::ProgressBar;
        use std::collections::HashMap;

        data.documents.extend(views::views());

        let mut revisions = HashMap::new();
        let total_attachments = data.attachments.len();
        let new_attachments = data
            .attachments
            .into_iter()
            .filter(|attachment| {
                !old_documents
                    .get(&attachment.id)
                    .map(|document| {
                        document
                            .attachments
                            .contains_key(&attachment.attachment_filename)
                    })
                    .unwrap_or_default()
            })
            .collect::<Vec<_>>();
        let total_documents = data.documents.len();
        let changed_documents = data
            .documents
            .into_iter()
            .filter_map(|mut new_doc| {
                let old_doc = old_documents.remove(&new_doc.id);
                if let Some(old_doc) = old_doc {
                    new_doc.revision.clone_from(&old_doc.revision);
                    new_doc.attachments.clone_from(&old_doc.attachments);

                    if old_doc == new_doc {
                        revisions.insert(new_doc.id, new_doc.revision.unwrap());
                        return None;
                    }
                }

                Some(new_doc)
            })
            .collect::<Vec<_>>();

        // Very imported to filter for certain types which we allow to be deleted!
        let deleted = old_documents
            .into_values()
            .filter(|document| {
                document.id.starts_with("private_event_")
                    || document.id.starts_with("public_event_")
            })
            .map(|mut document| {
                document.set_deleted();
                document
            })
            .collect::<Vec<_>>();
        println!("Deleting {} documents..", deleted.len());
        let pb = ProgressBar::new(deleted.len() as u64);
        for batch in deleted.chunks(100) {
            couchdb::save_batch(batch, &mut revisions)?;
            pb.inc(batch.len() as u64);
        }
        pb.finish();

        println!(
            "Creating/Updating {} of {} documents..",
            changed_documents.len(),
            total_documents
        );
        let pb = ProgressBar::new(changed_documents.len() as u64);
        for batch in changed_documents.chunks(100) {
            couchdb::save_batch(batch, &mut revisions)?;
            pb.inc(batch.len() as u64);
        }
        pb.finish();

        println!(
            "Uploading {} of {} files..",
            new_attachments.len(),
            total_attachments
        );
        let pb = ProgressBar::new(new_attachments.len() as u64);
        for attachment in new_attachments {
            couchdb::upload_attachment(
                &attachment.id,
                &attachment.attachment_filename,
                attachment.load()?,
                &mut revisions,
            )?;
            pb.inc(1);
        }
        pb.finish();
    }

    println!("Done");

    Ok(())
}
