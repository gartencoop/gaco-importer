use anyhow::{Context, Result};
use once_cell::sync::Lazy;
use reqwest::blocking::Client;
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use std::collections::HashMap;

static CLIENT: Lazy<Client> = Lazy::new(Client::new);

#[derive(Serialize, Deserialize, PartialEq, Eq, Debug)]
pub struct Document {
    #[serde(rename = "_id")]
    pub id: String,
    #[serde(skip_serializing_if = "Option::is_none", rename = "_rev")]
    pub revision: Option<String>,
    #[serde(
        skip_serializing_if = "HashMap::is_empty",
        rename = "_attachments",
        default
    )]
    pub attachments: HashMap<String, Value>,
    #[serde(default, rename = "_deleted", skip_serializing_if = "Option::is_none")]
    pub deleted: Option<bool>,
    #[serde(flatten)]
    pub extra: Value,
}

impl Document {
    pub fn set_deleted(&mut self) {
        self.deleted = Some(true);
        self.extra = Value::Null;
        self.attachments = HashMap::new();
    }
}

impl From<Value> for Document {
    fn from(val: Value) -> Self {
        serde_json::from_value(val).unwrap()
    }
}

pub fn get_all() -> Result<HashMap<String, Document>> {
    #[derive(Deserialize)]
    struct AllDocs {
        rows: Vec<AllDocsRow>,
    }

    #[derive(Deserialize)]
    struct AllDocsRow {
        doc: Document,
    }

    let res: AllDocs = CLIENT
        .get(format!(
            "{}/_all_docs?include_docs=true",
            crate::OPTS.couchdb_url
        ))
        .send()
        .context("Could not request _all_docs")?
        .error_for_status()?
        .json()
        .context("Could not load/parse json")?;

    let docs = res
        .rows
        .into_iter()
        .map(|row| (row.doc.id.clone(), row.doc))
        .collect();

    Ok(docs)
}

pub fn save_batch(batch: &[Document], revisions: &mut HashMap<String, String>) -> Result<()> {
    let res: Vec<CouchdbResult> = CLIENT
        .post(format!("{}/_bulk_docs", crate::OPTS.couchdb_url))
        .json(&json!({ "docs": batch }))
        .send()
        .context("Could not query _bulk_docs")?
        .error_for_status()?
        .json()
        .context("Could not load/parse json")?;

    for row in res.into_iter() {
        match row {
            CouchdbResult::Ok { id, rev } => {
                revisions.insert(id, rev);
            }
            CouchdbResult::Error { id, error, reason } => {
                anyhow::bail!("Could not save document \"{id}\": {error} - {reason}");
            }
        }
    }

    Ok(())
}

pub fn upload_attachment(
    id: &str,
    name: &str,
    data: Vec<u8>,
    revisions: &mut HashMap<String, String>,
) -> Result<()> {
    let revision = revisions
        .get(id)
        .ok_or_else(|| anyhow::format_err!("Could not find revision for attachment. id: {}", id))?;

    let res: CouchdbResult = CLIENT
        .put(format!(
            "{}/{id}/{name}?rev={revision}",
            crate::OPTS.couchdb_url
        ))
        .body(data)
        .send()
        .context("Could not upload attachment")?
        .error_for_status()?
        .json()
        .context("Could not load/parse json")?;

    match res {
        CouchdbResult::Ok { id, rev } => {
            revisions.insert(id, rev);
        }
        CouchdbResult::Error { id, error, reason } => {
            anyhow::bail!("Could not upload attachment for document \"{id}\": {error} - {reason}");
        }
    }

    Ok(())
}

#[derive(Deserialize)]
#[serde(untagged)]
enum CouchdbResult {
    Ok {
        id: String,
        rev: String,
    },
    Error {
        id: String,
        error: String,
        reason: String,
    },
}
