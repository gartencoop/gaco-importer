use super::{
    comments::CollectComments, files::CollectFiles, taxonomies::CollectTaxonomies,
    url_aliases::CollectUrlAliases,
};
use crate::{field_datetime, field_datetime_option, field_id, field_string_option, Data, Node};
use anyhow::Result;
use chrono::{DateTime, Duration, Utc};
use gaco_server::model::{
    event::{Category, Event},
    html::Html,
};
use log::debug;
use sqlparser::ast::Values;
use std::collections::BTreeMap;

#[derive(Default, Debug)]
pub struct CollectEvents {
    private: Vec<Node>,
    public: Vec<Node>,
    locations: BTreeMap<u32, BTreeMap<u32, ()>>,
    dates: BTreeMap<u32, (DateTime<Utc>, DateTime<Utc>)>,
    descriptions: BTreeMap<u32, String>,
    takealongs: BTreeMap<u32, String>,
    templates: BTreeMap<u32, u32>,
}

impl CollectEvents {
    pub fn add_node_private(&mut self, node: Node) {
        self.private.push(node);
    }

    pub fn add_node_public(&mut self, node: Node) {
        self.public.push(node);
    }

    /*
        "entity_type" varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
        "bundle" varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
        "deleted" tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
        "entity_id" int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
        "revision_id" int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
        "language" varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
        "delta" int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
        "field_meeting_location_tid" int(10) unsigned DEFAULT NULL,
    */
    pub fn parse_locations(&mut self, values: Values) -> Result<()> {
        for row in values.rows {
            let mut iter = row.into_iter().skip(3);
            field_id!(iter, entity_id);
            let entity_id = match entity_id {
                Some(entity_id) => entity_id,
                None => continue,
            };
            let mut iter = iter.skip(3);
            field_id!(iter, tid);
            if let Some(tid) = tid {
                self.locations.entry(entity_id).or_default().insert(tid, ());
            }
        }

        Ok(())
    }

    /*
        "entity_type" varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
        "bundle" varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
        "deleted" tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
        "entity_id" int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
        "revision_id" int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
        "language" varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
        "delta" int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
        "field_work_date_value" varchar(20) DEFAULT NULL,
        "field_work_date_value2" varchar(20) DEFAULT NULL,
    */
    pub fn parse_dates(&mut self, values: Values) -> Result<()> {
        for row in values.rows {
            let mut iter = row.into_iter().skip(3);
            field_id!(iter, entity_id);
            let entity_id = match entity_id {
                Some(entity_id) => entity_id,
                None => continue,
            };
            let mut iter = iter.skip(3);
            field_datetime!(iter, start);
            field_datetime_option!(iter, end);
            let end = match end {
                Some(end) => end,
                None => start + Duration::try_hours(1).expect("Could not create duration"),
            };
            self.dates.insert(entity_id, (start, end));
        }

        Ok(())
    }

    /*
        "entity_type" varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
        "bundle" varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
        "deleted" tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
        "entity_id" int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
        "revision_id" int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
        "language" varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
        "delta" int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
        "field_work_description_value" longtext,
        "field_work_description_summary" longtext,
        "field_work_description_format" varchar(255) DEFAULT NULL,
    */
    pub fn parse_descriptions(&mut self, values: Values) -> Result<()> {
        for row in values.rows {
            let mut iter = row.into_iter().skip(3);
            field_id!(iter, entity_id);
            let entity_id = match entity_id {
                Some(entity_id) => entity_id,
                None => continue,
            };
            let mut iter = iter.skip(3);
            field_string_option!(iter, value);
            if let Some(value) = value {
                self.descriptions.insert(entity_id, value);
            }
        }

        Ok(())
    }

    /*
        "entity_type" varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
        "bundle" varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
        "deleted" tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
        "entity_id" int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
        "revision_id" int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
        "language" varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
        "delta" int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
        "field_work_takealong_value" longtext,
        "field_work_takealong_format" varchar(255) DEFAULT NULL,
    */
    pub fn parse_takealongs(&mut self, values: Values) -> Result<()> {
        for row in values.rows {
            let mut iter = row.into_iter().skip(3);
            field_id!(iter, entity_id);
            let entity_id = match entity_id {
                Some(entity_id) => entity_id,
                None => continue,
            };
            let mut iter = iter.skip(3);
            field_string_option!(iter, value);
            if let Some(value) = value {
                self.takealongs.insert(entity_id, value);
            }
        }

        Ok(())
    }

    /*
        "entity_type" varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
        "bundle" varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
        "deleted" tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
        "entity_id" int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
        "revision_id" int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
        "language" varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
        "delta" int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
        "field_integrate_template_target_id" int(10) unsigned NOT NULL COMMENT 'The id of the target entity.',
    */
    pub fn parse_template(&mut self, values: Values) -> Result<()> {
        for row in values.rows {
            let mut iter = row.into_iter().skip(3);
            field_id!(iter, entity_id);
            let entity_id = match entity_id {
                Some(entity_id) => entity_id,
                None => continue,
            };
            let mut iter = iter.skip(3);
            field_id!(iter, template);
            if let Some(template) = template {
                self.templates.insert(entity_id, template);
            }
        }

        Ok(())
    }

    pub fn into_data(
        mut self,
        taxonomies: &CollectTaxonomies,
        url_aliases: &mut CollectUrlAliases,
        comments: &mut CollectComments,
        files: &mut CollectFiles,
    ) -> Result<Data> {
        let mut data = Data::default();

        for (private, node) in self
            .private
            .into_iter()
            .map(|node| (true, node))
            .chain(self.public.into_iter().map(|node| (false, node)))
        {
            let location = self
                .locations
                .remove(&node.nid)
                .unwrap_or_default()
                .into_keys()
                .chain(std::iter::once(33))
                .filter_map(|tid| taxonomies.get(tid))
                .map(|taxonomy| taxonomy.into())
                .next()
                .ok_or_else(|| anyhow::format_err!("Missing location"))?;

            let (start_at, end_at) = match self.dates.remove(&node.nid) {
                Some(dates) => dates,
                None => {
                    debug!("No work dates found for event {}", node.nid);
                    continue;
                }
            };
            let description = self
                .descriptions
                .remove(&node.nid)
                .map(|description| files.replace_links(description))
                .map(Html::parse);
            let takealong = self
                .takealongs
                .remove(&node.nid)
                .map(|description| files.replace_links(description))
                .map(Html::parse);
            let id = format!("{}_{}", Event::id_prefix(private), node.nid);
            let path = {
                let path = format!("node/{}", node.nid);
                match url_aliases.get(&path) {
                    Some(path) => path,
                    None => path,
                }
            };
            let template = self.templates.remove(&node.nid);
            let (comments, mut comments_attachments) = comments.get(node.nid, files, &id);
            let (files, mut attachments) = files.get(node.entity_id(), &id);

            let category = if node
                .title
                .to_lowercase()
                .contains("gemüse mit dem rad ausfahren")
            {
                Category::Fahrradverteilung
            } else if node.title.to_lowercase().contains("sharing") {
                Category::Sprinter
            } else if node.title.to_lowercase().contains("koko") {
                Category::Koko
            } else if node.title.to_lowercase().contains("ernte")
                || node.title.to_lowercase().contains("einsatz")
                || node.title.to_lowercase().contains("tunsel")
            {
                Category::Tunsel
            } else {
                Category::Sonstige
            };

            let event = Event::import(
                node.nid,
                private,
                path,
                node.language,
                node.title,
                node.created_at,
                node.created_by,
                description,
                takealong,
                location,
                category,
                start_at,
                end_at,
                template,
                files,
                comments,
            );

            data.documents.push(serde_json::to_value(event)?.into());
            data.attachments.append(&mut comments_attachments);
            data.attachments.append(&mut attachments);
        }

        Ok(data)
    }
}
