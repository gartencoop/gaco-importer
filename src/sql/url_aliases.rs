use crate::field_string;
use anyhow::Result;
use sqlparser::ast::Values;
use std::collections::BTreeMap;

#[derive(Default, Debug)]
pub struct CollectUrlAliases {
    aliases: BTreeMap<String, String>,
}

impl CollectUrlAliases {
    /*
        "pid" int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'A unique path alias identifier.',
        "source" varchar(255) NOT NULL DEFAULT '' COMMENT 'The Drupal path this alias is for; e.g. node/12.',
        "alias" varchar(255) NOT NULL DEFAULT '' COMMENT 'The alias for this path; e.g. title-of-the-story.',
        "language" varchar(12) NOT NULL DEFAULT '' COMMENT 'The language this alias is for; if ’und’, the alias will be used for unknown languages. Each Drupal path can have an alias for each supported language.',
    */
    pub fn parse(&mut self, values: Values) -> Result<()> {
        for row in values.rows {
            let mut iter = row.into_iter().skip(1);
            field_string!(iter, source);
            field_string!(iter, alias);

            self.aliases.insert(source, alias);
        }

        Ok(())
    }

    pub fn get(&mut self, source: &str) -> Option<String> {
        self.aliases.remove(source)
    }
}
