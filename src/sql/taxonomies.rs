use crate::{field_id, field_string, field_string_option, Taxonomy};
use anyhow::Result;
use sqlparser::ast::Values;
use std::collections::BTreeMap;

#[derive(Default, Debug)]
pub struct CollectTaxonomies {
    terms: BTreeMap<u32, Taxonomy>,
    vocabularies: BTreeMap<u32, Vec<Taxonomy>>,
}

impl CollectTaxonomies {
    /*
        "tid" int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key: Unique term ID.',
        "vid" int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The taxonomy_vocabulary.vid of the vocabulary to which the term is assigned.',
        "name" varchar(255) NOT NULL DEFAULT '' COMMENT 'The term name.',
        "description" longtext COMMENT 'A description of the term.',
        "format" varchar(255) DEFAULT NULL COMMENT 'The filter_format.format of the description.',
        "weight" int(11) NOT NULL DEFAULT '0' COMMENT 'The weight of this term in relation to other terms.',
    */
    pub fn parse(&mut self, values: Values) -> Result<()> {
        for row in values.rows {
            let mut iter = row.into_iter();
            field_id!(iter, tid);
            let tid = match tid {
                Some(tid) => tid,
                None => continue,
            };
            field_id!(iter, vid);
            let vid = match vid {
                Some(vid) => vid,
                None => continue,
            };
            field_string!(iter, name);
            field_string_option!(iter, description);

            let description = description.filter(|str| str.is_empty()).map(|description| {
                if let Some(stripped_description) = description
                    .strip_prefix("<p>")
                    .and_then(|desc| desc.strip_suffix("</p>\\r\\n"))
                {
                    stripped_description.to_string()
                } else {
                    description
                }
            });

            let taxonomy = Taxonomy::import(name, description);
            self.vocabularies
                .entry(vid)
                .or_default()
                .push(taxonomy.clone());
            self.terms.insert(tid, taxonomy);
        }

        Ok(())
    }

    pub fn get(&self, tid: u32) -> Option<Taxonomy> {
        self.terms.get(&tid).cloned()
    }
}
