use super::{
    documents::CollectDocuments, files::CollectFiles, taxonomies::CollectTaxonomies,
    url_aliases::CollectUrlAliases,
};
use crate::{field_id, field_string, Data, Node};
use anyhow::Result;
use gaco_server::model::vp::{VPStatus, VP};
use sqlparser::ast::Values;
use std::collections::BTreeMap;

#[derive(Default, Debug)]
pub struct CollectVPs {
    nodes: Vec<Node>,
    codes: BTreeMap<u32, String>,
    status: BTreeMap<u32, u32>,
    contacts: BTreeMap<u32, String>,
    case_storages: BTreeMap<u32, String>,
    openings: BTreeMap<u32, String>,
    locations: BTreeMap<u32, String>,
}

impl CollectVPs {
    pub fn add_node(&mut self, node: Node) {
        self.nodes.push(node);
    }

    /*
        "entity_type" varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
        "bundle" varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
        "deleted" tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
        "entity_id" int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
        "revision_id" int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
        "language" varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
        "delta" int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
        "field_distribition_code_value" varchar(255) DEFAULT NULL,
        "field_distribition_code_format" varchar(255) DEFAULT NULL,
    */
    pub fn parse_code(&mut self, values: Values) -> Result<()> {
        for row in values.rows {
            let mut iter = row.into_iter().skip(3);
            field_id!(iter, entity_id);
            let entity_id = match entity_id {
                Some(entity_id) => entity_id,
                None => continue,
            };
            let mut iter = iter.skip(3);
            field_string!(iter, code);
            self.codes.insert(entity_id, code);
        }

        Ok(())
    }

    /*
        "entity_type" varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
        "bundle" varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
        "deleted" tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
        "entity_id" int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
        "revision_id" int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
        "language" varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
        "delta" int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
        "field_distribition_status_tid" int(10) unsigned DEFAULT NULL,
    */
    pub fn parse_status(&mut self, values: Values) -> Result<()> {
        for row in values.rows {
            let mut iter = row.into_iter().skip(3);
            field_id!(iter, entity_id);
            let entity_id = match entity_id {
                Some(entity_id) => entity_id,
                None => continue,
            };
            let mut iter = iter.skip(3);
            field_id!(iter, tid);
            if let Some(tid) = tid {
                if self.status.insert(entity_id, tid).is_some() {
                    anyhow::bail!("Status was already set!");
                };
            }
        }

        Ok(())
    }

    /*
        "entity_type" varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
        "bundle" varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
        "deleted" tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
        "entity_id" int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
        "revision_id" int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
        "language" varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
        "delta" int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
        "field_distribition_contact_value" varchar(2550) DEFAULT NULL,
        "field_distribition_contact_format" varchar(255) DEFAULT NULL,
    */
    pub fn parse_contact(&mut self, values: Values) -> Result<()> {
        for row in values.rows {
            let mut iter = row.into_iter().skip(3);
            field_id!(iter, entity_id);
            let entity_id = match entity_id {
                Some(entity_id) => entity_id,
                None => continue,
            };
            let mut iter = iter.skip(3);
            field_string!(iter, contact);
            self.contacts.insert(entity_id, contact);
        }

        Ok(())
    }

    /*
        "entity_type" varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
        "bundle" varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
        "deleted" tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
        "entity_id" int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
        "revision_id" int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
        "language" varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
        "delta" int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
        "field_distribution_casestorage_value" varchar(255) DEFAULT NULL,
        "field_distribution_casestorage_format" varchar(255) DEFAULT NULL,
    */
    pub fn parse_casestorage(&mut self, values: Values) -> Result<()> {
        for row in values.rows {
            let mut iter = row.into_iter().skip(3);
            field_id!(iter, entity_id);
            let entity_id = match entity_id {
                Some(entity_id) => entity_id,
                None => continue,
            };
            let mut iter = iter.skip(3);
            field_string!(iter, contact);
            self.case_storages.insert(entity_id, contact);
        }

        Ok(())
    }

    /*
        "entity_type" varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
        "bundle" varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
        "deleted" tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
        "entity_id" int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
        "revision_id" int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
        "language" varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
        "delta" int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
        "field_distribution_opening_value" varchar(255) DEFAULT NULL,
        "field_distribution_opening_format" varchar(255) DEFAULT NULL,
    */
    pub fn parse_opening(&mut self, values: Values) -> Result<()> {
        for row in values.rows {
            let mut iter = row.into_iter().skip(3);
            field_id!(iter, entity_id);
            let entity_id = match entity_id {
                Some(entity_id) => entity_id,
                None => continue,
            };
            let mut iter = iter.skip(3);
            field_string!(iter, opening);
            self.openings.insert(entity_id, opening);
        }

        Ok(())
    }

    /*
        "entity_type" varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
        "bundle" varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
        "deleted" tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
        "entity_id" int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
        "revision_id" int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
        "language" varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
        "delta" int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
        "field_distribution_space_value" varchar(255) DEFAULT NULL,
        "field_distribution_space_format" varchar(255) DEFAULT NULL,
    */
    pub fn parse_space(&mut self, values: Values) -> Result<()> {
        for row in values.rows {
            let mut iter = row.into_iter().skip(3);
            field_id!(iter, entity_id);
            let entity_id = match entity_id {
                Some(entity_id) => entity_id,
                None => continue,
            };
            let mut iter = iter.skip(3);
            field_string!(iter, space);
            self.locations.insert(entity_id, space);
        }

        Ok(())
    }

    pub fn into_data(
        mut self,
        taxonomies: &CollectTaxonomies,
        url_aliases: &mut CollectUrlAliases,
        files: &mut CollectFiles,
        documents: &mut CollectDocuments,
    ) -> Result<Data> {
        let mut data = Data::default();

        for node in self.nodes.into_iter() {
            let code = self
                .codes
                .remove(&node.nid)
                .unwrap_or_else(|| node.title.clone());
            let status = match self
                .status
                .remove(&node.nid)
                .and_then(|tid| taxonomies.get(tid))
                .unwrap_or_default()
                .into_name()
                .as_str()
            {
                "fix" => VPStatus::Operational,
                "möglich" => VPStatus::Possible,
                "unmöglich" => VPStatus::Impossible,
                _ => VPStatus::Unknown,
            };
            let contact = self.contacts.remove(&node.nid).unwrap_or_default();
            let case_storage = self.case_storages.remove(&node.nid).unwrap_or_default();
            let opening = self.openings.remove(&node.nid).unwrap_or_default();
            let location = self.locations.remove(&node.nid).unwrap_or_default();
            let path = {
                let path = format!("node/{}", node.nid);
                match url_aliases.get(&path) {
                    Some(path) => path,
                    None => path,
                }
            };
            let body = documents.body(files, node.nid, node.revision);

            let doc_id = format!("{}_{}", VP::ID_PREFIX, node.nid);
            let (files, mut attachments) = files.get(node.entity_id(), &doc_id);

            let vp = VP::import(
                node.nid,
                path,
                node.language,
                node.title,
                node.created_at,
                node.created_by,
                body,
                code,
                status,
                contact,
                case_storage,
                opening,
                location,
                files,
            );

            data.documents.push(serde_json::to_value(vp)?.into());
            data.attachments.append(&mut attachments);
        }
        Ok(data)
    }
}
