#[macro_export]
macro_rules! next_string {
    ($iter: ident) => {
        $iter
            .next()
            .and_then(|expr| match expr {
                sqlparser::ast::Expr::Value(
                    sqlparser::ast::Value::SingleQuotedString(value)
                    | sqlparser::ast::Value::DoubleQuotedString(value),
                ) => Some(
                    value
                        .replace("<p>", "")
                        .replace("</p>", "")
                        .replace("\\r\\n", "\n")
                        .replace("<br />", "\n")
                        .trim()
                        .to_owned(),
                ),
                sqlparser::ast::Expr::Value(sqlparser::ast::Value::Null) => None,
                sqlparser::ast::Expr::TypedString { value, .. } => Some(value.trim().to_owned()),
                value => {
                    log::error!("unexpected expr value: {value:?}");
                    None
                }
            })
            .filter(|str| !str.is_empty() && str != "NULL")
    };
}

#[macro_export]
macro_rules! next_parsed_string {
    ($iter: ident, $type: tt) => {
        $crate::next_string!($iter)
            .map(|value| value.parse::<$type>())
            .transpose()?
    };
}

#[macro_export]
macro_rules! field_string {
    ($iter: ident, $field: ident) => {
        let $field = $crate::next_string!($iter).ok_or_else(|| {
            anyhow::format_err!("{} is in unsupported format", stringify!($field))
        })?;
    };
}

#[macro_export]
macro_rules! field_string_option {
    ($iter: ident, $field: ident) => {
        let $field = $crate::next_string!($iter);
    };
}

#[macro_export]
macro_rules! field_number_u32 {
    ($iter: ident, $field: ident) => {
        let $field = $iter.next().and_then(|expr| match expr {
            sqlparser::ast::Expr::Value(sqlparser::ast::Value::Number(value, false)) => {
                value.parse::<u32>().ok()
            }
            sqlparser::ast::Expr::Value(sqlparser::ast::Value::Null) => None,
            value => {
                log::error!("unexpected expr value: {value:?}");
                None
            }
        });
    };
}

#[macro_export]
macro_rules! field_number_f32 {
    ($iter: ident, $field: ident) => {
        let $field = $iter.next().and_then(|expr| match expr {
            sqlparser::ast::Expr::Value(sqlparser::ast::Value::Number(value, false)) => {
                value.parse::<f32>().ok()
            }
            sqlparser::ast::Expr::UnaryOp {
                op: sqlparser::ast::UnaryOperator::Minus,
                expr,
            } => match *expr {
                sqlparser::ast::Expr::Value(sqlparser::ast::Value::Number(value, false)) => {
                    value.parse::<f32>().ok().map(|value| value * -1.0)
                }
                expr => {
                    log::error!("unexpected unaryop expr value: {expr:?}");
                    None
                }
            },
            sqlparser::ast::Expr::Value(sqlparser::ast::Value::Null) => None,
            value => {
                log::error!("unexpected expr value: {value:?}");
                None
            }
        });
    };
}

#[macro_export]
macro_rules! field_id {
    ($iter: ident, $field: ident) => {
        let $field = $crate::next_parsed_string!($iter, u32).filter(|id| *id != 0);
    };
}

#[macro_export]
macro_rules! field_entity_id {
    ($iter: ident, $field: ident) => {
        let $field = {
            $crate::field_string!($iter, ty);
            $iter.next();
            $iter.next();
            field_id!($iter, id);

            id.map(|id| match ty.as_str() {
                "node" => EntityId::Node(id),
                "comment" => EntityId::Comment(id),
                ty => panic!("Entity type not supported: {ty}"),
            })
        };
    };
}

#[macro_export]
macro_rules! field_bool {
    ($iter: ident, $field: ident) => {
        let $field = $crate::next_string!($iter)
            .map(|str| str.as_str() == "1")
            .ok_or_else(|| {
                anyhow::format_err!("{} is in unsupported format", stringify!($field))
            })?;
    };
}

#[macro_export]
macro_rules! field_datetime_timestamp {
    ($iter: ident, $field: ident) => {
        let $field = chrono::TimeZone::from_utc_datetime(
            &chrono::Utc,
            &$crate::next_parsed_string!($iter, i64)
                .and_then(|timestamp| {
                    chrono::DateTime::from_timestamp(timestamp, 0).map(|dt| dt.naive_utc())
                })
                .ok_or_else(|| {
                    anyhow::format_err!("{} is in unsupported format", stringify!($field))
                })?,
        );
    };
}

#[macro_export]
macro_rules! field_date_option {
    ($iter: ident, $field: ident) => {
        let $field =
            $crate::next_string!($iter).and_then(|str| gaco_server::common::parse_date(&str));
    };
}

#[macro_export]
macro_rules! field_date {
    ($iter: ident, $field: ident) => {
        let $field = $crate::next_string!($iter)
            .and_then(|str| gaco_server::common::parse_date(&str))
            .expect(&format!("Could not parse date: {}", stringify!($field)));
    };
}

#[macro_export]
macro_rules! field_datetime {
    ($iter: ident, $field: ident) => {
        let $field = {
            let str = $crate::next_string!($iter).ok_or_else(|| {
                anyhow::format_err!("{} is in unsupported format", stringify!($field))
            })?;
            use anyhow::Context;
            chrono::TimeZone::from_utc_datetime(
                &chrono::Utc,
                &chrono::NaiveDateTime::parse_from_str(&str.replace('T', ""), "%Y-%m-%d%H:%M:%S")
                    .with_context(|| format!("Could not parse string datetime: \"{str}\""))?,
            )
        };
    };
}

#[macro_export]
macro_rules! field_datetime_option {
    ($iter: ident, $field: ident) => {
        let $field = $crate::next_string!($iter).and_then(|str| {
            use anyhow::Context;
            Some(chrono::TimeZone::from_utc_datetime(
                &chrono::Utc,
                &chrono::NaiveDateTime::parse_from_str(&str.replace('T', ""), "%Y-%m-%d%H:%M:%S")
                    .with_context(|| format!("Could not parse string datetime: \"{str}\""))
                    .ok()?,
            ))
        });
    };
}

#[macro_export]
macro_rules! field_datetime_str {
    ($iter: ident, $field: ident) => {
        let $field = {
            let str = $crate::next_string!($iter).ok_or_else(|| {
                anyhow::format_err!("{} is in unsupported format", stringify!($field))
            })?;
            use anyhow::Context;
            chrono::DateTime::<chrono::Utc>::from_utc(
                chrono::NaiveDateTime::parse_from_str(&str.replace('T', ""), "%Y-%m-%d%H:%M:%S")
                    .with_context(|| format!("Could not parse string datetime: \"{str}\""))?,
                chrono::Utc,
            )
            .to_rfc3339()
        };
    };
}

#[macro_export]
macro_rules! field_datetime_str_option {
    ($iter: ident, $field: ident) => {
        let $field = $crate::next_string!($iter)
            .map(|str| {
                use anyhow::Context;
                chrono::NaiveDateTime::parse_from_str(&str.replace('T', ""), "%Y-%m-%d%H:%M:%S")
                    .with_context(|| format!("Could not parse string datetime: \"{str}\""))
                    .map(|dt| {
                        chrono::DateTime::<chrono::Utc>::from_utc(dt, chrono::Utc).to_rfc3339()
                    })
            })
            .transpose()?;
    };
}

#[macro_export]
macro_rules! field_language {
    ($iter: ident, $field: ident) => {
        let $field = match $crate::next_string!($iter).as_deref() {
            Some("de") | None => Ok(gaco_server::model::language::Language::German),
            Some("en") => Ok(gaco_server::model::language::Language::English),
            Some(lang) => Err(anyhow::format_err!("Language not supported: {lang}")),
        }?;
    };
}
