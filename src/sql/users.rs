use crate::{
    couchdb::Document, field_bool, field_datetime_timestamp, field_id, field_language,
    field_string, Data,
};
use anyhow::Result;
use gaco_server::{
    couchdb::CouchdbDocument,
    model::user::{AccessLevel, User},
};
use serde::Deserialize;
use sqlparser::ast::Values;
use std::collections::{BTreeMap, HashMap};

#[derive(Default, Debug)]
pub struct CollectUsers {
    users: Vec<(u32, User)>,
    names: BTreeMap<u32, String>,
    phones: BTreeMap<u32, String>,
}

impl CollectUsers {
    /*
        "uid" int(10) unsigned NOT NULL DEFAULT '0',
        "name" varchar(60) NOT NULL DEFAULT '' COMMENT 'Unique user name.',
        "pass" varchar(128) NOT NULL DEFAULT '' COMMENT 'User’s password (hashed).',
        "mail" varchar(254) DEFAULT '' COMMENT 'User’s e-mail address.',
        "theme" varchar(255) NOT NULL DEFAULT '' COMMENT 'User’s default theme.',
        "signature" varchar(255) NOT NULL DEFAULT '' COMMENT 'User’s signature.',
        "signature_format" varchar(255) DEFAULT NULL COMMENT 'The filter_format.format of the signature.',
        "created" int(11) NOT NULL DEFAULT '0' COMMENT 'Timestamp for when user was created.',
        "access" int(11) NOT NULL DEFAULT '0' COMMENT 'Timestamp for previous time user accessed the site.',
        "login" int(11) NOT NULL DEFAULT '0' COMMENT 'Timestamp for user’s last login.',
        "status" tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Whether the user is active(1) or blocked(0).',
        "timezone" varchar(32) DEFAULT NULL COMMENT 'User’s time zone.',
        "language" varchar(12) NOT NULL DEFAULT '' COMMENT 'User’s default language.',
        "picture" int(11) NOT NULL DEFAULT '0' COMMENT 'Foreign key: file_managed.fid of user’s picture.',
        "init" varchar(254) DEFAULT '' COMMENT 'E-mail address used for initial account creation.',
        "data" longblob COMMENT 'A serialized array of name value pairs that are related to the user. Any form values posted during user edit are stored and are loaded into the $user object during user_load(). Use of this field is discouraged and it will likely disappear in a future...',
    */
    pub fn parse_users(&mut self, values: Values) -> Result<()> {
        for row in values.rows {
            let mut iter = row.into_iter();
            field_id!(iter, uid);
            let uid = match uid {
                Some(uid) => uid,
                None => continue,
            };
            field_string!(iter, username);
            field_string!(iter, password);
            field_string!(iter, email);
            let email = email.to_lowercase();
            let mut iter = iter.skip(3);
            field_datetime_timestamp!(iter, created_at);
            let mut iter = iter.skip(2);
            field_bool!(iter, status);
            if !status {
                continue;
            }
            let mut iter = iter.skip(1);
            field_language!(iter, language);

            self.users.push((
                uid,
                User::import(
                    uid,
                    username,
                    password,
                    created_at,
                    email,
                    language,
                    if uid == 909 {
                        Some("acb547f1-2d51-4f95-94b8-eb07f1e20613".to_owned())
                    } else {
                        None
                    },
                    match uid {
                        12 | 279 | 909 | 481 | 3506 => Some(AccessLevel::Admin), // 12: Fabzgy, 279: Luciano_admin, 909: René, 481: Heike, 3506: Maize
                        61 | 243 | 1503 => Some(AccessLevel::Manager), // 61: Mel, 243: bocaj, 1503: nad
                        _ => None,
                    },
                ),
            ));
        }

        Ok(())
    }

    /*
        "entity_type" varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
        "bundle" varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
        "deleted" tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
        "entity_id" int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
        "revision_id" int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
        "language" varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
        "delta" int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
        "field_account_name_value" varchar(255) DEFAULT NULL,
        "field_account_name_format" varchar(255) DEFAULT NULL,
    */
    pub fn parse_account_names(&mut self, values: Values) -> Result<()> {
        for row in values.rows {
            let mut iter = row.into_iter().skip(3);
            field_id!(iter, entity_id);
            let entity_id = match entity_id {
                Some(entity_id) => entity_id,
                None => continue,
            };
            let mut iter = iter.skip(3);
            field_string!(iter, name);
            self.names.insert(entity_id, name);
        }

        Ok(())
    }

    /*
        "entity_type" varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
        "bundle" varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
        "deleted" tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
        "entity_id" int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
        "revision_id" int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
        "language" varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
        "delta" int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
        "field_account_phone_value" varchar(255) DEFAULT NULL,
        "field_account_phone_format" varchar(255) DEFAULT NULL,
    */
    pub fn parse_account_phones(&mut self, values: Values) -> Result<()> {
        for row in values.rows {
            let mut iter = row.into_iter().skip(3);
            field_id!(iter, entity_id);
            let entity_id = match entity_id {
                Some(entity_id) => entity_id,
                None => continue,
            };
            let mut iter = iter.skip(3);
            field_string!(iter, phone);
            if let Some(phone) = crate::phone::format(phone) {
                self.phones.insert(entity_id, phone);
            }
        }

        Ok(())
    }

    pub fn into_data(mut self, old_documents: &HashMap<String, Document>) -> Result<Data> {
        #[cfg(feature = "csv")]
        {
            use std::io::Write;

            let mut csv = std::fs::File::create("/home/rene/Downloads/benutzer.csv")
                .expect("Could not create csv file");
            csv.write_all("Benutzernummer;Benutzername;Name;Email;Tel\n".as_bytes())
                .expect("Could not write csv file");
            for (uid, user) in self.users.iter() {
                csv.write_all(
                    format!(
                        "{};{};{};{};{}\n",
                        uid,
                        user.username(),
                        self.names
                            .get(uid)
                            .map(|name| name.to_owned())
                            .unwrap_or_default(),
                        user.email(),
                        self.phones
                            .get(uid)
                            .map(|phone| phone.to_owned())
                            .unwrap_or_default()
                    )
                    .as_bytes(),
                )
                .expect("Could not write csv file");
            }

            csv.flush().expect("Could not flush csv file");
            drop(csv);

            log::info!("created benutzer.csv");
        }

        let mut data = Data::default();

        for (uid, mut user) in self.users.into_iter() {
            if let Some(name) = self.names.remove(&uid) {
                user.set_name(name);
            }
            if let Some(phone) = self.phones.remove(&uid) {
                user.set_phone(phone);
            }

            // Keep some data
            if let Some(old_user) = old_documents.get(user.meta().id()) {
                #[derive(Deserialize)]
                struct OldUser {
                    feed_token: Option<String>,
                    password_token: Option<String>,
                }
                let old_user: OldUser = serde_json::from_value(old_user.extra.clone()).unwrap();
                user.set_feed_token(old_user.feed_token);
                user.set_password_token(old_user.password_token);
            }

            data.documents.push(serde_json::to_value(&user)?.into());
        }

        Ok(data)
    }
}
