use super::files::CollectFiles;
use crate::{
    field_datetime_timestamp, field_id, field_string, field_string_option, Attachment, EntityId,
};
use anyhow::Result;
use chrono::{DateTime, Utc};
use gaco_server::model::{comment::Comment, html::Html};
use sqlparser::ast::Values;
use std::collections::BTreeMap;

type Comments = BTreeMap<u32, Vec<(u32, Option<String>, u32, DateTime<Utc>)>>;

#[derive(Default, Debug)]
pub struct CollectComments {
    comments: Comments,
    bodies: BTreeMap<u32, String>,
}

impl CollectComments {
    /*
        "cid" int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key: Unique comment ID.',
        "pid" int(11) NOT NULL DEFAULT '0' COMMENT 'The comment.cid to which this comment is a reply. If set to 0, this comment is not a reply to an existing comment.',
        "nid" int(11) NOT NULL DEFAULT '0' COMMENT 'The node.nid to which this comment is a reply.',
        "uid" int(11) NOT NULL DEFAULT '0' COMMENT 'The users.uid who authored the comment. If set to 0, this comment was created by an anonymous user.',
        "subject" varchar(64) NOT NULL DEFAULT '' COMMENT 'The comment title.',
        "hostname" varchar(128) NOT NULL DEFAULT '' COMMENT 'The author’s host name.',
        "created" int(11) NOT NULL DEFAULT '0' COMMENT 'The time that the comment was created, as a Unix timestamp.',
        "changed" int(11) NOT NULL DEFAULT '0' COMMENT 'The time that the comment was last edited, as a Unix timestamp.',
        "status" tinyint(3) unsigned NOT NULL DEFAULT '1',
        "thread" varchar(255) NOT NULL COMMENT 'The vancode representation of the comment’s place in a thread.',
        "name" varchar(60) DEFAULT NULL COMMENT 'The comment author’s name. Uses users.name if the user is logged in, otherwise uses the value typed into the comment form.',
        "mail" varchar(64) DEFAULT NULL COMMENT 'The comment author’s e-mail address from the comment form, if user is anonymous, and the ’Anonymous users may/must leave their contact information’ setting is turned on.',
        "homepage" varchar(255) DEFAULT NULL COMMENT 'The comment author’s home page address from the comment form, if user is anonymous, and the ’Anonymous users may/must leave their contact information’ setting is turned on.',
        "language" varchar(12) NOT NULL DEFAULT '' COMMENT 'The languages.language of this comment.',
    */
    pub fn parse_comment(&mut self, values: Values) -> Result<()> {
        for row in values.rows {
            let mut iter = row.into_iter();
            field_id!(iter, cid);
            let cid = match cid {
                Some(cid) => cid,
                None => continue,
            };
            let mut iter = iter.skip(1);
            field_id!(iter, nid);
            let nid = match nid {
                Some(nid) => nid,
                None => continue,
            };
            field_id!(iter, created_by);
            let created_by = match created_by {
                Some(created_by) => created_by,
                None => continue,
            };
            field_string_option!(iter, subject);
            let mut iter = iter.skip(1);
            field_datetime_timestamp!(iter, created_at);

            self.comments
                .entry(nid)
                .or_default()
                .push((cid, subject, created_by, created_at));
        }

        Ok(())
    }

    /*
        "entity_type" varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
        "bundle" varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
        "deleted" tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
        "entity_id" int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
        "revision_id" int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
        "language" varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
        "delta" int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
        "comment_body_value" longtext,
        "comment_body_format" varchar(255) DEFAULT NULL,
    */
    pub fn parse_body(&mut self, values: Values) -> Result<()> {
        for row in values.rows {
            let mut iter = row.into_iter().skip(3);
            field_id!(iter, entity_id);
            let entity_id = match entity_id {
                Some(entity_id) => entity_id,
                None => continue,
            };
            let mut iter = iter.skip(3);
            field_string!(iter, body);

            self.bodies.insert(entity_id, body);
        }

        Ok(())
    }

    pub fn get(
        &mut self,
        nid: u32,
        files: &mut CollectFiles,
        doc_id: &str,
    ) -> (Vec<Comment>, Vec<Attachment>) {
        let mut attachments = vec![];

        let comments = self
            .comments
            .remove(&nid)
            .map(|comments| {
                comments
                    .into_iter()
                    .filter_map(|(cid, subject, created_by, created_at)| {
                        self.bodies.remove(&cid).map(|body| {
                            let (files, mut comment_attachments) =
                                files.get(EntityId::comment(cid), doc_id);
                            attachments.append(&mut comment_attachments);
                            let body = Html::parse(match subject {
                                Some(subject) => format!("{subject}\n{body}"),
                                None => body,
                            });
                            Comment::import(created_by, created_at, body, files)
                        })
                    })
                    .collect()
            })
            .unwrap_or_default();

        (comments, attachments)
    }
}
