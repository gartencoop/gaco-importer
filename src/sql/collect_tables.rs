use super::{
    comments::CollectComments, documents::CollectDocuments, events::CollectEvents,
    files::CollectFiles, taxonomies::CollectTaxonomies, url_aliases::CollectUrlAliases,
    users::CollectUsers, vps::CollectVPs,
};
use crate::{
    couchdb::Document, field_bool, field_datetime_timestamp, field_id, field_language,
    field_string, Data, Node,
};
use anyhow::{Context, Result};
use log::info;
use sqlparser::ast::{Insert, SetExpr, Statement, TableObject, Values};
use std::collections::HashMap;

#[derive(Default, Debug)]
pub struct CollectTables {
    taxonomies: CollectTaxonomies,
    url_aliases: CollectUrlAliases,
    comments: CollectComments,
    files: CollectFiles,
    users: CollectUsers,
    events: CollectEvents,
    vps: CollectVPs,
    documents: CollectDocuments,
}

impl CollectTables {
    /*
        "nid" int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'The primary identifier for a node.',
        "vid" int(10) unsigned DEFAULT NULL COMMENT 'The current node_revision.vid version identifier.',
        "type" varchar(32) NOT NULL DEFAULT '' COMMENT 'The node_type.type of this node.',
        "language" varchar(12) NOT NULL DEFAULT '' COMMENT 'The languages.language of this node.',
        "title" varchar(255) NOT NULL DEFAULT '' COMMENT 'The title of this node, always treated as non-markup plain text.',
        "uid" int(11) NOT NULL DEFAULT '0' COMMENT 'The users.uid that owns this node; initially, this is the user that created it.',
        "status" int(11) NOT NULL DEFAULT '1' COMMENT 'Boolean indicating whether the node is published (visible to non-administrators).',
        "created" int(11) NOT NULL DEFAULT '0' COMMENT 'The Unix timestamp when the node was created.',
        "changed" int(11) NOT NULL DEFAULT '0' COMMENT 'The Unix timestamp when the node was most recently saved.',
        "comment" int(11) NOT NULL DEFAULT '0' COMMENT 'Whether comments are allowed on this node: 0 = no, 1 = closed (read only), 2 = open (read/write).',
        "promote" int(11) NOT NULL DEFAULT '0' COMMENT 'Boolean indicating whether the node should be displayed on the front page.',
        "sticky" int(11) NOT NULL DEFAULT '0' COMMENT 'Boolean indicating whether the node should be displayed at the top of lists in which it appears.',
        "tnid" int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The translation set id for this node, which equals the node id of the source post in each set.',
        "translate" int(11) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this translation page needs to be updated.',
    */
    fn parse_nodes(&mut self, values: Values) -> Result<()> {
        for row in values.rows {
            let mut iter = row.into_iter();
            field_id!(iter, nid);
            let nid = match nid {
                Some(6469) | None => continue,
                Some(nid) => nid,
            };
            field_id!(iter, revision);
            let revision = match revision {
                Some(revision) => revision,
                None => panic!("Missing revision"),
            };
            let ty = {
                field_string!(iter, ty);

                match nid {
                    6468 | 6437 | 5635 | 5381 | 4742 => "internal_docs".to_string(),
                    4698 => "offtopic".to_string(),
                    _ => ty,
                }
            };

            field_language!(iter, language);
            field_string!(iter, title);
            field_id!(iter, created_by);
            let created_by = created_by.unwrap_or(0);
            field_bool!(iter, status);
            if !status {
                continue;
            }
            field_datetime_timestamp!(iter, created_at);

            let node = Node {
                nid,
                revision,
                language,
                title,
                created_by,
                created_at,
            };

            match ty.as_str() {
                "article" => self.documents.add_node_news(node),
                "event" => self.events.add_node_private(node),
                "internal_docs" => self.documents.add_node_internals(node),
                "newswire" => self.documents.add_node_medias(node),
                "offtopic" => self.documents.add_node_offtopics(node),
                "page" => self.documents.add_node_pages(node),
                "public_event" => self.events.add_node_public(node),
                "template" => self.documents.add_node_templates(node),
                "verteilpunkt" => self.vps.add_node(node),
                "webform" | "webform_report" | "we_need" => (),
                name => {
                    panic!("Node type not implemented: {name}")
                }
            }
        }
        Ok(())
    }

    pub fn collect() -> Result<Self> {
        let mut collect_tables = CollectTables::default();

        let ast = super::statements()?;
        info!("Constructed ast iterator from sql dump");

        for statements in ast {
            for statement in statements? {
                if let Statement::Insert(Insert {
                    table: TableObject::TableName(table_name),
                    source: Some(source),
                    ..
                }) = statement
                {
                    if let SetExpr::Values(values) = *source.body {
                        match table_name.0[0].value.as_str() {
                            "users" => collect_tables
                                .users
                                .parse_users(values)
                                .context("Could not parse user")?,
                            "field_data_field_account_name" => collect_tables
                                .users
                                .parse_account_names(values)
                                .context("Could not parse account name")?,
                            "field_data_field_account_phone" => collect_tables
                                .users
                                .parse_account_phones(values)
                                .context("Could not parse account phone")?,
                            "node" => collect_tables
                                .parse_nodes(values)
                                .context("Could not parse node")?,
                            "taxonomy_term_data" => collect_tables
                                .taxonomies
                                .parse(values)
                                .context("Could not parse taxonomy terms")?,
                            "field_data_field_meeting_location" => collect_tables
                                .events
                                .parse_locations(values)
                                .context("Could not parse meeting locations")?,
                            "field_data_field_work_date" => collect_tables
                                .events
                                .parse_dates(values)
                                .context("Could not parse work dates")?,
                            "field_data_field_work_description" => collect_tables
                                .events
                                .parse_descriptions(values)
                                .context("Could not parse work descriptions")?,
                            "field_data_field_work_takealong" => collect_tables
                                .events
                                .parse_takealongs(values)
                                .context("Could not parse work takealongs")?,
                            "field_data_field_distribition_code" => collect_tables
                                .vps
                                .parse_code(values)
                                .context("Could not parse distribition code")?,
                            "field_data_field_distribition_status" => collect_tables
                                .vps
                                .parse_status(values)
                                .context("Could not parse distribition code")?,
                            "field_data_field_distribition_contact" => collect_tables
                                .vps
                                .parse_contact(values)
                                .context("Could not parse distribition contact")?,
                            "field_data_field_distribution_casestorage" => collect_tables
                                .vps
                                .parse_casestorage(values)
                                .context("Could not parse distribition casestorage")?,
                            "field_data_field_distribution_opening" => collect_tables
                                .vps
                                .parse_opening(values)
                                .context("Could not parse distribition opening")?,
                            "field_data_field_distribution_space" => collect_tables
                                .vps
                                .parse_space(values)
                                .context("Could not parse distribition space")?,
                            "url_alias" => collect_tables
                                .url_aliases
                                .parse(values)
                                .context("Could not parse url alias")?,
                            "comment" => collect_tables
                                .comments
                                .parse_comment(values)
                                .context("Could not parse comment")?,
                            "field_data_comment_body" => collect_tables
                                .comments
                                .parse_body(values)
                                .context("Could not parse comment body")?,
                            "file_managed" => collect_tables
                                .files
                                .parse_store(values)
                                .context("Could not parse file")?,
                            "field_data_field_file" => collect_tables
                                .files
                                .parse_file(values)
                                .context("Could not parse file reference")?,
                            "field_data_field_image" => collect_tables
                                .files
                                .parse_image(values)
                                .context("Could not parse image reference")?,
                            "field_data_field_audio" => collect_tables
                                .files
                                .parse_audio(values)
                                .context("Could not parse audio reference")?,
                            "field_data_body" => collect_tables
                                .documents
                                .parse_body(values)
                                .context("Could not parse body")?,
                            "field_data_field_newswire_abstract" => collect_tables
                                .documents
                                .parse_newswire_abstract(values)
                                .context("Could not parse newswire abstract")?,
                            "field_data_field_newswire_text" => collect_tables
                                .documents
                                .parse_newswire_text(values)
                                .context("Could not parse newswire text")?,
                            "field_revision_field_docs_group" => collect_tables
                                .documents
                                .parse_group(values)
                                .context("Could not parse group")?,
                            "field_data_field_docs_public_topic"
                            | "field_data_field_docs_topic" => collect_tables
                                .documents
                                .parse_topic(values)
                                .context("Could not parse tag")?,
                            "field_data_field_integrate_template" => collect_tables
                                .events
                                .parse_template(values)
                                .context("Could not parse template")?,
                            _name => {
                                //println!("missing {name}")
                            }
                        }
                    }
                }
            }
        }

        Ok(collect_tables)
    }

    pub fn into_data(mut self, old_documents: &HashMap<String, Document>) -> Result<Data> {
        let mut data = Data::default();

        let mut events = self.events.into_data(
            &self.taxonomies,
            &mut self.url_aliases,
            &mut self.comments,
            &mut self.files,
        )?;
        let mut vps = self.vps.into_data(
            &self.taxonomies,
            &mut self.url_aliases,
            &mut self.files,
            &mut self.documents,
        )?;
        let mut documents =
            self.documents
                .into_data(&self.taxonomies, &mut self.url_aliases, &mut self.files)?;
        let mut users = self.users.into_data(old_documents)?;

        data.documents.append(&mut events.documents);
        data.documents.append(&mut documents.documents);
        data.documents.append(&mut users.documents);
        data.documents.append(&mut vps.documents);

        data.attachments.append(&mut events.attachments);
        data.attachments.append(&mut documents.attachments);
        data.attachments.append(&mut users.attachments);
        data.attachments.append(&mut vps.attachments);

        Ok(data)
    }
}
