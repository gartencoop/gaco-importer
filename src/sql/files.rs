use crate::{field_entity_id, field_id, field_string_option, Attachment, EntityId};
use anyhow::Result;
use gaco_server::model::file::File;
use once_cell::sync::Lazy;
use rand_pcg::Pcg64;
use rand_seeder::rand_core::RngCore;
use regex::{Captures, Regex};
use sqlparser::ast::Values;
use std::collections::BTreeMap;

#[derive(Debug)]
struct StoreFile {
    created_by: u32,
    name: String,
    store_filename: String,
    attachment_filename: String,
}

#[derive(Default, Debug)]
pub struct CollectFiles {
    store: BTreeMap<u32, StoreFile>,
    files: BTreeMap<EntityId, Vec<u32>>,
    store_to_attachment: BTreeMap<String, (u32, String)>,
}

impl CollectFiles {
    /*
        "fid" int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'File ID.',
        "uid" int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The users.uid of the user who is associated with the file.',
        "filename" varchar(255) NOT NULL DEFAULT '' COMMENT 'Name of the file with no path components. This may differ from the basename of the URI if the file is renamed to avoid overwriting an existing file.',
        "uri" varchar(255) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL DEFAULT '' COMMENT 'The URI to access the file (either local or remote).',
        "filemime" varchar(255) NOT NULL DEFAULT '' COMMENT 'The file’s MIME type.',
        "filesize" bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'The size of the file in bytes.',
        "status" tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A field indicating the status of the file. Two status are defined in core: temporary (0) and permanent (1). Temporary files older than DRUPAL_MAXIMUM_TEMP_FILE_AGE will be removed during a cron run.',
        "timestamp" int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'UNIX timestamp for when the file was added.',
    */
    pub fn parse_store(&mut self, values: Values) -> Result<()> {
        for row in values.rows {
            let mut iter = row.into_iter();
            field_id!(iter, fid);
            let fid = match fid {
                Some(fid) => fid,
                None => continue,
            };
            field_id!(iter, created_by);
            field_string_option!(iter, name);
            field_string_option!(iter, uri);

            let store_filename = match uri.clone().and_then(|uri| {
                uri.strip_prefix("private://")
                    .map(|store_name| store_name.to_string())
            }) {
                Some(store_filename) => store_filename,
                None => {
                    continue;
                }
            };
            let name = name.unwrap_or_else(|| store_filename.clone());

            // Missing files:
            if let "A4_halb.pdf"
            | "4_kapitel-Auswertung_Messergebnisse.pdf"
            | " Budget kurz vor Jahresmitte.pdf"
            | "12_07_02_prognose_budget_0.pdf"
            | "12_10_01_finanzupdate_0.pdf"
            | "10_Gruende_DINA3.pdf"
            | "1306Bauernstimme_Seite9-CSAFreiburg-KrummeGurken_0.pdf"
            | "l und Za\"tar_0.pdf"
            | "2012-01-19_BrauereiGanter.pdf" = store_filename.as_str()
            {
                continue;
            }

            let mut rng: Pcg64 = rand_seeder::Seeder::from(store_filename.clone()).make_rng();
            let mut uuid_bytes = [0u8; 16];
            rng.fill_bytes(&mut uuid_bytes);

            let attachment_filename = uuid::Uuid::from_bytes(uuid_bytes).to_string();
            self.store_to_attachment
                .insert(store_filename.clone(), (fid, attachment_filename.clone()));
            self.store.insert(
                fid,
                StoreFile {
                    created_by: created_by.unwrap_or_default(),
                    name,
                    store_filename,
                    attachment_filename,
                },
            );
        }

        Ok(())
    }

    /*
        "entity_type" varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
        "bundle" varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
        "deleted" tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
        "entity_id" int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
        "revision_id" int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
        "language" varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
        "delta" int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
        "field_file_fid" int(10) unsigned DEFAULT NULL COMMENT 'The file_managed.fid being referenced in this field.',
        "field_file_display" tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT 'Flag to control whether this file should be displayed when viewing content.',
        "field_file_description" mediumtext COMMENT 'A description of the file.',
    */
    pub fn parse_file(&mut self, values: Values) -> Result<()> {
        for row in values.rows {
            let mut iter = row.into_iter();
            field_entity_id!(iter, entity_id);
            let entity_id = match entity_id {
                Some(entity_id) => entity_id,
                None => continue,
            };
            let mut iter = iter.skip(3);
            field_id!(iter, fid);
            let fid = match fid {
                Some(fid) => fid,
                None => continue,
            };
            self.files.entry(entity_id).or_default().push(fid);
        }

        Ok(())
    }

    /*
        "entity_type" varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
        "bundle" varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
        "deleted" tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
        "entity_id" int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
        "revision_id" int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
        "language" varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
        "delta" int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
        "field_image_fid" int(10) unsigned DEFAULT NULL COMMENT 'The file_managed.fid being referenced in this field.',
        "field_image_alt" varchar(512) DEFAULT NULL,
        "field_image_title" varchar(1024) DEFAULT NULL,
        "field_image_width" int(10) unsigned DEFAULT NULL COMMENT 'The width of the image in pixels.',
        "field_image_height" int(10) unsigned DEFAULT NULL COMMENT 'The height of the image in pixels.',
    */
    pub fn parse_image(&mut self, values: Values) -> Result<()> {
        for row in values.rows {
            let mut iter = row.into_iter();
            field_entity_id!(iter, entity_id);
            let entity_id = match entity_id {
                Some(entity_id) => entity_id,
                None => continue,
            };
            let mut iter = iter.skip(3);
            field_id!(iter, fid);
            let fid = match fid {
                Some(fid) => fid,
                None => continue,
            };
            self.files.entry(entity_id).or_default().push(fid);
        }

        Ok(())
    }

    /*
        "entity_type" varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
        "bundle" varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
        "deleted" tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
        "entity_id" int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
        "revision_id" int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
        "language" varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
        "delta" int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
        "field_audio_fid" int(10) unsigned DEFAULT NULL COMMENT 'The file_managed.fid being referenced in this field.',
    */
    pub fn parse_audio(&mut self, values: Values) -> Result<()> {
        for row in values.rows {
            let mut iter = row.into_iter();
            field_entity_id!(iter, entity_id);
            let entity_id = match entity_id {
                Some(entity_id) => entity_id,
                None => continue,
            };
            let mut iter = iter.skip(3);
            field_id!(iter, fid);
            let fid = match fid {
                Some(fid) => fid,
                None => continue,
            };
            self.files.entry(entity_id).or_default().push(fid);
        }

        Ok(())
    }

    pub fn replace_links(&self, text: String) -> String {
        static FILEURL_REGEX: Lazy<Regex> = Lazy::new(|| {
            Regex::new(
                r#"(?m)"http[s]?://(?:www\.)?gartencoop.org/tunsel/(?:system/)files/([^"]*)(?:#.*)?""#,
            )
            .unwrap()
        });

        FILEURL_REGEX
            .replace_all(&text, |captures: &Captures| {
                match self.replace_link(captures) {
                    Some(replaced) => replaced,
                    None => captures[0].to_string(),
                }
            })
            //TODO: Remove!
            .replace("www.gartencoop.org", "gaco.uber.space")
            .replace("gartencoop.org", "gaco.uber.space")
    }

    fn replace_link(&self, captures: &Captures) -> Option<String> {
        let store_name = urlencoding::decode(&captures[1]).unwrap();
        let (fid, attachment_name) = self.store_to_attachment.get(store_name.as_ref())?;
        let nid = self
            .files
            .iter()
            .find(|(_entity_id, fids)| fids.contains(fid))
            .and_then(|(entity_id, _fids)| entity_id.nid())?;

        Some(format!(
            //TODO: Change to gartencoop.org!
            "https://gaco.uber.space/file/{nid}/{attachment_name}"
        ))
    }

    pub fn get(&mut self, entity_id: EntityId, doc_id: &str) -> (Vec<File>, Vec<Attachment>) {
        let mut attachments = vec![];
        let files = self
            .files
            .get(&entity_id)
            .map(|fids| {
                fids.iter()
                    .filter_map(|fid| self.store.remove(fid))
                    .map(|store_file| {
                        let attachment = Attachment::new(
                            doc_id.to_string(),
                            store_file.store_filename,
                            store_file.attachment_filename,
                        );
                        let file = File::import(
                            store_file.created_by,
                            store_file.name,
                            attachment.attachment_filename.clone(),
                        );
                        attachments.push(attachment);

                        file
                    })
                    .collect()
            })
            .unwrap_or_default();

        (files, attachments)
    }
}
