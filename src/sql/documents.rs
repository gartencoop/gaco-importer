use super::{files::CollectFiles, taxonomies::CollectTaxonomies, url_aliases::CollectUrlAliases};
use crate::{field_bool, field_id, field_string_option, Data, Node};
use anyhow::Result;
use gaco_server::model::{
    document::{Document, DocumentType, Team, Topic},
    html::Html,
};
use sqlparser::ast::Values;
use std::collections::{BTreeMap, HashSet};

#[derive(Default, Debug)]
pub struct CollectDocuments {
    nodes: Vec<(Node, DocumentType)>,
    abstracts: BTreeMap<(u32, u32), String>,
    bodies: BTreeMap<(u32, u32), String>,
    teams: BTreeMap<(u32, u32), HashSet<u32>>,
    tags: BTreeMap<(u32, u32), HashSet<u32>>,
}

impl CollectDocuments {
    pub fn add_node_news(&mut self, node: Node) {
        self.nodes.push((node, DocumentType::News));
    }

    pub fn add_node_medias(&mut self, node: Node) {
        self.nodes.push((node, DocumentType::Media));
    }

    pub fn add_node_internals(&mut self, node: Node) {
        self.nodes.push((node, DocumentType::Internal));
    }

    pub fn add_node_offtopics(&mut self, node: Node) {
        self.nodes.push((node, DocumentType::Offtopic));
    }

    pub fn add_node_pages(&mut self, node: Node) {
        self.nodes.push((node, DocumentType::Page));
    }

    pub fn add_node_templates(&mut self, node: Node) {
        self.nodes.push((node, DocumentType::Template));
    }

    /*
        "entity_type" varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
        "bundle" varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
        "deleted" tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
        "entity_id" int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
        "revision_id" int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
        "language" varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
        "delta" int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
        "body_value" longtext,
        "body_summary" longtext,
        "body_format" varchar(255) DEFAULT NULL,
    */
    pub fn parse_body(&mut self, values: Values) -> Result<()> {
        for row in values.rows {
            let mut iter = row.into_iter().skip(3);
            field_id!(iter, entity_id);
            let entity_id = match entity_id {
                Some(entity_id) => entity_id,
                None => continue,
            };
            field_id!(iter, revision_id);
            let revision_id = match revision_id {
                Some(revision_id) => revision_id,
                None => panic!("Not versioned"),
            };
            let mut iter = iter.skip(2);
            field_string_option!(iter, body);

            if let Some(body) = body {
                self.bodies.insert((entity_id, revision_id), body);
            }
        }

        Ok(())
    }

    /*
        `entity_type` varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
        `bundle` varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
        `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
        `entity_id` int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
        `revision_id` int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
        `language` varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
        `delta` int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
        `field_newswire_abstract_value` longtext,
        `field_newswire_abstract_format` varchar(255) DEFAULT NULL,
    */
    pub fn parse_newswire_abstract(&mut self, values: Values) -> Result<()> {
        for row in values.rows {
            let mut iter = row.into_iter().skip(3);
            field_id!(iter, entity_id);
            let entity_id = match entity_id {
                Some(entity_id) => entity_id,
                None => continue,
            };
            field_id!(iter, revision_id);
            let revision_id = match revision_id {
                Some(revision_id) => revision_id,
                None => panic!("Not versioned"),
            };
            let mut iter = iter.skip(2);
            field_string_option!(iter, body);
            if let Some(body) = body {
                self.abstracts.insert((entity_id, revision_id), body);
            }
        }

        Ok(())
    }

    /*
        "entity_type" varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
        "bundle" varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
        "deleted" tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
        "entity_id" int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
        "revision_id" int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
        "language" varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
        "delta" int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
        "field_newswire_text_value" longtext,
        "field_newswire_text_format" varchar(255) DEFAULT NULL,
    */
    pub fn parse_newswire_text(&mut self, values: Values) -> Result<()> {
        for row in values.rows {
            let mut iter = row.into_iter().skip(3);
            field_id!(iter, entity_id);
            let entity_id = match entity_id {
                Some(entity_id) => entity_id,
                None => continue,
            };
            field_id!(iter, revision_id);
            let revision_id = match revision_id {
                Some(revision_id) => revision_id,
                None => panic!("Not versioned"),
            };
            let mut iter = iter.skip(2);
            field_string_option!(iter, body);
            if let Some(body) = body {
                self.bodies.insert((entity_id, revision_id), body);
            }
        }

        Ok(())
    }

    /*
        "entity_type" varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
        "bundle" varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
        "deleted" tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
        "entity_id" int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
        "revision_id" int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
        "language" varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
        "delta" int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',
        "field_docs_group_tid" int(10) unsigned DEFAULT NULL,
    */
    pub fn parse_group(&mut self, values: Values) -> Result<()> {
        for row in values.rows {
            let mut iter = row.into_iter().skip(2);
            field_bool!(iter, deleted);
            if deleted {
                continue;
            }
            field_id!(iter, entity_id);
            let entity_id = match entity_id {
                Some(entity_id) => entity_id,
                None => continue,
            };
            field_id!(iter, revision_id);
            let revision_id = match revision_id {
                Some(revision_id) => revision_id,
                None => panic!("Not versioned"),
            };
            let mut iter = iter.skip(2);
            field_id!(iter, tid);
            if let Some(tid) = tid {
                self.teams
                    .entry((entity_id, revision_id))
                    .or_default()
                    .insert(tid);
            }
        }

        Ok(())
    }

    /*
        "entity_type" varchar(128) NOT NULL DEFAULT '' COMMENT 'The entity type this data is attached to',
        "bundle" varchar(128) NOT NULL DEFAULT '' COMMENT 'The field instance bundle to which this row belongs, used when deleting a field instance',
        "deleted" tinyint(4) NOT NULL DEFAULT '0' COMMENT 'A boolean indicating whether this data item has been deleted',
        "entity_id" int(10) unsigned NOT NULL COMMENT 'The entity id this data is attached to',
        "revision_id" int(10) unsigned DEFAULT NULL COMMENT 'The entity revision id this data is attached to, or NULL if the entity type is not versioned',
        "language" varchar(32) NOT NULL DEFAULT '' COMMENT 'The language for this data item.',
        "delta" int(10) unsigned NOT NULL COMMENT 'The sequence number for this data item, used for multi-value fields',

        "field_docs_public_topic_tid" int(10) unsigned DEFAULT NULL,
         - OR -
        "field_docs_topic_tid" int(10) unsigned DEFAULT NULL,
    */
    pub fn parse_topic(&mut self, values: Values) -> Result<()> {
        for row in values.rows {
            let mut iter = row.into_iter().skip(3);
            field_id!(iter, entity_id);
            let entity_id = match entity_id {
                Some(entity_id) => entity_id,
                None => continue,
            };
            field_id!(iter, revision_id);
            let revision_id = match revision_id {
                Some(revision_id) => revision_id,
                None => panic!("Not versioned"),
            };
            let mut iter = iter.skip(2);
            field_id!(iter, tid);
            if let Some(tid) = tid {
                self.tags
                    .entry((entity_id, revision_id))
                    .or_default()
                    .insert(tid);
            }
        }

        Ok(())
    }

    pub fn body(&mut self, files: &mut CollectFiles, nid: u32, revision: u32) -> Html {
        match (
            self.abstracts.remove(&(nid, revision)),
            self.bodies.remove(&(nid, revision)),
        ) {
            (None, None) => None,
            (None, Some(body)) => Some(body),
            (Some(abs), None) => Some(abs),
            (Some(abs), Some(body)) => Some(format!("{abs}<br><br>{body}")),
        }
        .map(|body| files.replace_links(body))
        .map(Html::parse)
        .unwrap_or_default()
    }

    pub fn into_data(
        mut self,
        taxonomies: &CollectTaxonomies,
        url_aliases: &mut CollectUrlAliases,
        files: &mut CollectFiles,
    ) -> Result<Data> {
        let mut data = Data::default();

        #[allow(clippy::needless_collect)]
        let nodes = self.nodes.drain(..).collect::<Vec<_>>();
        for (node, ty) in nodes.into_iter() {
            let id = format!("{}_{}", Document::id_prefix(&ty), node.nid);

            let path = {
                let path = format!("node/{}", node.nid);
                match url_aliases.get(&path) {
                    Some(path) => path,
                    None => path,
                }
            };
            let mut teams = self
                .teams
                .remove(&(node.nid, node.revision))
                .unwrap_or_default()
                .into_iter()
                .filter_map(|tid| taxonomies.get(tid))
                .map(|taxonomy| taxonomy.into_name())
                .collect::<Vec<_>>();
            teams.sort();
            let teams = teams
                .iter()
                .map(|team| Team::try_from(team.as_str()).expect("Could not find team"))
                .collect::<Vec<_>>();
            let mut topics = self
                .tags
                .remove(&(node.nid, node.revision))
                .unwrap_or_default()
                .into_iter()
                .filter_map(|tid| taxonomies.get(tid))
                .map(|taxonomy| taxonomy.into_name())
                .collect::<Vec<_>>();
            topics.sort();
            let topic = topics.first().and_then(|topic| Topic::from(topic));
            let body = self.body(files, node.nid, node.revision);

            let (mut files, mut attachments) = files.get(node.entity_id(), &id);
            files.sort();

            let document = Document::import(
                node.nid,
                ty,
                path,
                node.title,
                node.created_at,
                node.created_by,
                teams,
                topic,
                body,
                files,
            );

            data.documents.push(serde_json::to_value(document)?.into());
            data.attachments.append(&mut attachments);
        }

        Ok(data)
    }
}
