use crate::couchdb::Document;
use anyhow::Result;
use chrono::{DateTime, Utc};
use gaco_server::model::language::Language;

#[derive(Debug)]
pub struct Node {
    pub nid: u32,
    pub revision: u32,
    pub language: Language,
    pub title: String,
    pub created_at: DateTime<Utc>,
    pub created_by: u32,
}

impl Node {
    pub fn entity_id(&self) -> EntityId {
        EntityId::node(self.nid)
    }
}

#[derive(Debug, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub enum EntityId {
    Node(u32),
    Comment(u32),
}

impl EntityId {
    pub fn node(nid: u32) -> Self {
        Self::Node(nid)
    }

    pub fn comment(cid: u32) -> Self {
        Self::Comment(cid)
    }

    pub fn nid(&self) -> Option<u32> {
        match self {
            Self::Node(nid) => Some(*nid),
            _ => None,
        }
    }
}

#[derive(Default)]
pub struct Data {
    pub documents: Vec<Document>,
    pub attachments: Vec<Attachment>,
}

pub struct Attachment {
    pub id: String,
    pub attachment_filename: String,
    store_filename: String,
}

impl Attachment {
    pub fn new(id: String, store_filename: String, attachment_filename: String) -> Self {
        Self {
            id,
            attachment_filename,
            store_filename,
        }
    }

    pub fn load(&self) -> Result<Vec<u8>> {
        let store_path = crate::OPTS
            .store_path
            .join("tunsel")
            .join("files")
            .join(&self.store_filename);
        if let Ok(data) = std::fs::read(store_path) {
            return Ok(data);
        }

        let store_path = crate::OPTS
            .store_path
            .join("tunsel")
            .join("files")
            .join("styles")
            .join("large")
            .join("private")
            .join(&self.store_filename);
        if let Ok(data) = std::fs::read(store_path) {
            return Ok(data);
        }

        let store_path = crate::OPTS
            .store_path
            .join("tunsel")
            .join("files")
            .join("styles")
            .join("medium")
            .join("private")
            .join(&self.store_filename);
        if let Ok(data) = std::fs::read(store_path) {
            return Ok(data);
        }

        Err(anyhow::format_err!(
            "Could not find file: {}",
            self.store_filename
        ))
    }
}
