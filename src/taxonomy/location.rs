use super::Taxonomy;
use gaco_server::model::event::Location;

impl From<Taxonomy> for Location {
    fn from(taxonomy: Taxonomy) -> Self {
        /*
        let taxonomy = taxonomy.into_tuple();
        Self {
            name: taxonomy.0,
            description: taxonomy.1,
        }
        */
        Self(taxonomy.into_name())
    }
}
