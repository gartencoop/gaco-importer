use super::Taxonomy;

impl Taxonomy {
    pub fn import(name: String, description: Option<String>) -> Self {
        Self { name, description }
    }
}
