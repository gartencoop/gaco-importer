pub fn format(phone: String) -> Option<String> {
    if let "" | "0" | "NULL" | "Nen Teufel werd ich tun!" | "habe keine" | "HectorJohnson" =
        phone.as_str()
    {
        return None;
    }

    let phone_sanitized = phone
        .replace(['-', ' ', '/', ','], "")
        .replace("(0)", "")
        .replace("H:", "")
        .replace("Festnetz:", "")
        .replace("Handy:", "")
        .replace("J:", "")
        .replace("julian@gartencoop.org", "")
        .replace(['o', '.'], "");
    let mut phone_sanitized = phone_sanitized
        .trim_start_matches('0')
        .trim_start_matches('+')
        .to_owned();

    if phone_sanitized.len() < 8 {
        phone_sanitized = format!("49761{phone_sanitized}")
    } else if !phone_sanitized.starts_with("49") {
        phone_sanitized = format!("49{phone_sanitized}")
    }

    let number = match phone_sanitized.parse::<u64>() {
        Ok(number) => number,
        Err(_) => {
            panic!("Could not parse phone number: \"{phone}\", sanitized: \"{phone_sanitized}\"")
        }
    };
    Some(format!("+{number}"))
}
