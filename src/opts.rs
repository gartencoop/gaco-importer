use clap::Parser;
use once_cell::sync::Lazy;
use std::path::PathBuf;

pub static OPTS: Lazy<Opts> = Lazy::new(Opts::parse);

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about = None)]
pub struct Opts {
    /// Path to www.gartencoop.org folder
    pub store_path: PathBuf,

    /// Url of couchdb database
    pub couchdb_url: String,
}
