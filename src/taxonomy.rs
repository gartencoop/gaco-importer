mod import;
mod location;

use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Default, Deserialize, Serialize)]
#[serde(default)]
pub struct Taxonomy {
    name: String,
    description: Option<String>,
}

impl Taxonomy {
    pub fn into_tuple(self) -> (String, Option<String>) {
        (self.name, self.description)
    }

    pub fn into_name(self) -> String {
        self.name
    }
}
