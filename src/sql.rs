mod collect_tables;
mod comments;
mod common;
mod documents;
mod events;
mod files;
mod macros;
mod taxonomies;
mod url_aliases;
mod users;
mod vps;

use crate::couchdb::Document;

use self::collect_tables::CollectTables;
use anyhow::{Context, Result};
pub use common::*;
use flate2::read::GzDecoder;
use log::info;
use sqlparser::{ast::Statement, dialect::GenericDialect, parser::Parser};
use std::{collections::HashMap, io::Read};

pub fn statements() -> Result<Box<dyn Iterator<Item = Result<Vec<Statement>>>>> {
    let backup_path = crate::OPTS
        .store_path
        .join("tunsel")
        .join("files")
        .join("backup_migrate")
        .join("scheduled");
    let entries = std::fs::read_dir(backup_path).context("Could not list sql files")?;

    let mut sql_path = None;
    for entry in entries {
        let path = entry?.path();
        if path
            .extension()
            .and_then(|extension| extension.to_str().map(|str| str == "gz"))
            .unwrap_or_default()
        {
            match sql_path {
                None => sql_path = Some(path),
                Some(old_path) if old_path < path => sql_path = Some(path),
                _ => (),
            }
        }
    }
    let path = sql_path.ok_or_else(|| anyhow::format_err!("Could not find latest sql file"))?;
    info!("Using database backup: {}", path.display());

    let filtered_sql: Vec<String> = {
        let sql_file = std::fs::File::open(path)?;
        let mut decoder = GzDecoder::new(sql_file);
        let mut sql = String::new();
        decoder.read_to_string(&mut sql)?;
        sql.lines()
            .filter(|line| line.starts_with("INSERT INTO"))
            .map(|line| line.replace('`', "\""))
            .collect()
    };

    info!("Constructing ast iterator..");
    Ok(Box::new(filtered_sql.into_iter().map(|line| {
        Parser::parse_sql(&GenericDialect {}, &line).map_err(anyhow::Error::msg)
    })))
}

pub fn jverein_statements() -> Result<Box<dyn Iterator<Item = Result<Vec<Statement>>>>> {
    let path = crate::OPTS.store_path.join("backup.sql");
    info!("Using jverein database backup: {}", path.display());

    #[allow(clippy::needless_collect)]
    let sql_lines = std::fs::read_to_string(path)
        .context("Could not read jverein database backup file")?
        .lines()
        .map(|line| line.trim())
        .collect::<Vec<_>>()
        .join("\n")
        .replace("\"PUBLIC\".", "")
        .replace(";\n", "SEMICOLONNEWLINE")
        .replace('\n', "")
        .replace("SEMICOLONNEWLINE", ";\n")
        .replace("PUBLIC.", "")
        .lines()
        .filter(|line| line.trim().starts_with("INSERT"))
        .map(|line| line.to_string())
        .collect::<Vec<_>>();

    info!("Constructing ast iterator..");
    Ok(Box::new(sql_lines.into_iter().map(|line| {
        Parser::parse_sql(&GenericDialect {}, &line).map_err(anyhow::Error::msg)
    })))
}

pub fn data(old_documents: &HashMap<String, Document>) -> Result<Data> {
    CollectTables::collect()?.into_data(old_documents)
}
