set -e

cargo build --release --target x86_64-unknown-linux-musl
rsync -vz ../target/x86_64-unknown-linux-musl/release/gaco-importer gaco@erinome.uberspace.de:
ssh gaco@erinome.uberspace.de "mv gaco-importer bin/gaco-importer-test"
