#!/bin/bash
set -e
source .env

supervisorctl stop gaco-server-live
/home/gaco/bin/gaco-importer /home/gaco/www.gartencoop.org http://admin:$COUCHDB_PASS@localhost:5984/gaco-live
supervisorctl start gaco-server-live

supervisorctl stop gaco-server-test
/home/gaco/bin/gaco-importer /home/gaco/www.gartencoop.org http://admin:$COUCHDB_PASS@localhost:5984/gaco-test
supervisorctl start gaco-server-test