#!/bin/bash
set -e
source .env

SOURCEFOLDER='/'
TARGETFOLDER='/home/gaco/www.gartencoop.org'

lftp -f "
open $FTP_HOST
user '$FTP_USER' '$FTP_PASS'
lcd $TARGETFOLDER
mirror --delete --verbose $SOURCEFOLDER $TARGETFOLDER
bye
" || true

/home/gaco/import.sh